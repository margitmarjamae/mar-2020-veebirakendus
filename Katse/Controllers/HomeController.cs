﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Katse.Controllers
{
    public class HomeController : Controller
    {

        public string Tere(string id) => $"Tere {id}!!";

        public ActionResult Sipsik()
        {
            ViewBag.Sõbrad = new string[] { "Margit", "Ants", "Peeter" }; 
            return View();
        }

        public ActionResult Index()
        {
            return View(); // mis view on, on views kaustas
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Katse.Startup))]
namespace Katse
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TestVacAppKodus.Startup))]
namespace TestVacAppKodus
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
